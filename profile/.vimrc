nnoremap d "_d
nnoremap D "_D
vnoremap d "_d
vnoremap D "_D

set noerrorbells visualbell t_vb=
aucocmd GUIEnter * set visualbell t_vb=

set foldmethod=indent   
set foldnestmax=10
"set nofoldenable
set foldlevel=0

nnoremap x "_x
nnoremap X "_x
vnoremap x "_x
vnoremap X "_x
set encoding=utf-8

" CTRL-X and SHIFT-Del are Cut
vnoremap <C-X> "+x
vnoremap <S-Del> "+x

" CTRL-C and CTRL-Insert are Copy
vnoremap <C-C> "+y
vnoremap <C-Insert> "+y

" CTRL-V and SHIFT-Insert are Paste
map <C-V>       "+gP
map <S-Insert>      "+gP

cmap <C-V>      <C-R>+
cmap <S-Insert>     <C-R>+

" Pasting blockwise and linewise selections is not possible in Insert and
" Visual mode without the +virtualedit feature.  They are pasted as if they
" were characterwise instead.
" Uses the paste.vim autoload script.

exe 'inoremap <script> <C-V>' paste#paste_cmd['i']
exe 'vnoremap <script> <C-V>' paste#paste_cmd['v']

imap <S-Insert>     <C-V>
vmap <S-Insert>     <C-V>

" Use CTRL-Q to do what CTRL-V used to do
noremap <C-Q>       <C-V>

set backspace=indent,eol,start
set tabstop=2 shiftwidth=2 expandtab
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')
Plugin 'nathanaelkane/vim-indent-guides'
" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"vim plugin

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.vim/plugged')
" On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tpope/vim-fireplace', { 'for': 'clojure' }

" Using git URL
Plug 'https://github.com/junegunn/vim-github-dashboard.git'

" Plugin options
Plug 'nsf/gocode', { 'tag': 'go.weekly.2012-03-13', 'rtp': 'vim' }

" Plugin outside ~/.vim/plugged with post-update hook
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': 'yes \| ./install' }

" Unmanaged plugin (manually installed and updated)
Plug '~/my-prototype-plugin'

call plug#end()

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"python powershell #
call setreg("p", "I#j"," v")

"html <!--  -->
call setreg("h", "I<!--A-->j"," v")
"l - html  
"call setreg("l", 
"js //
call setreg("j", "I//j"," v")

"css /*  */
call setreg("c", "I/*A*/j"," v")

"call setreg("b", "I#j"," v")
"call setreg("b", "I#j"," v")
" crtl+ q is the escape char for inputting the commands in the keyboard
" %{}  --> evaluate the command
" %( %)  -->bracket to group the command
" \  -->escape
" %t --> tail(end)
" %:p:h  --> the head of full path 
" expand() --> eval of the meta char
set number
set titlestring=%t%(\ [%M]%)\ %{expand(\"%:p:h\")}\ Powered\ by\ Sam
"set titlestring=%t%(\ %M%)%(\ (%{expand(\"%:p:h\")})%)%(\ %a%)\ -\ %{v:servername}

set noerrorbells visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb

"if has("unnamedplus")
	"set clipboard = unnamed, unnamedplus
	"nnoremap <leader>d "+d"
	"nnoremap <leader>D "+D"
	"vnoremap <leader>d "+d"
"else 
	"set clipboard=unnamed
	"nnoremap <leader>d "*d
	"nnoremap <leader>D "*D
	"vnoremap <leader>d "*d
"endif
